<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="X-USB" urn="urn:adsk.eagle:library:17541912">
<packages>
<package name="USB-A-PCB" urn="urn:adsk.eagle:footprint:17541913/1" locally_modified="yes" library_version="2" library_locally_modified="yes">
<smd name="P$1" x="0.5" y="3.5" dx="7.41" dy="1" layer="1"/>
<smd name="P$2" x="0" y="1" dx="6.41" dy="1" layer="1"/>
<smd name="P$3" x="0" y="-1" dx="6.41" dy="1" layer="1"/>
<smd name="P$4" x="0.5" y="-3.5" dx="7.41" dy="1" layer="1"/>
<wire x1="-4.55" y1="6.15" x2="5.45" y2="6.15" width="0.127" layer="51"/>
<wire x1="5.45" y1="6.15" x2="5.45" y2="-5.85" width="0.127" layer="51"/>
<wire x1="5.45" y1="-5.85" x2="-4.55" y2="-5.85" width="0.127" layer="51"/>
<wire x1="-4.55" y1="-5.85" x2="-4.55" y2="6.15" width="0.127" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="USB-A-PCB" urn="urn:adsk.eagle:package:17541915/2" type="empty" library_version="2" library_locally_modified="yes">
<packageinstances>
<packageinstance name="USB-A-PCB"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="4PIN" urn="urn:adsk.eagle:symbol:17541914/1" library_version="2">
<pin name="VBUS" x="0" y="0"/>
<pin name="D-" x="0" y="-5.08"/>
<pin name="D+" x="0" y="-10.16"/>
<pin name="GND" x="0" y="-15.24"/>
<wire x1="7.62" y1="0" x2="5.08" y2="1.524" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="5.08" y2="-1.524" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="5.08" y2="-3.556" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="5.08" y2="-6.604" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="5.08" y2="-8.636" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="5.08" y2="-11.684" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="5.08" y2="-13.716" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="5.08" y2="-16.764" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="3.556" y1="0" x2="3.556" y2="-15.24" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB-A-PCB" urn="urn:adsk.eagle:component:17541916/2" locally_modified="yes" prefix="X" library_version="2" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="4PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB-A-PCB">
<connects>
<connect gate="G$1" pin="D+" pad="P$3"/>
<connect gate="G$1" pin="D-" pad="P$2"/>
<connect gate="G$1" pin="GND" pad="P$4"/>
<connect gate="G$1" pin="VBUS" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17541915/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="R" urn="urn:adsk.eagle:library:17502134">
<packages>
<package name="RESC1608X50N" urn="urn:adsk.eagle:footprint:17469637/1" library_version="4" library_locally_modified="yes">
<description>Chip, 1.60 X 0.80 X 0.50 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.50 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.764" x2="-0.85" y2="0.764" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.764" x2="-0.85" y2="-0.764" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.425" x2="-0.85" y2="-0.425" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.425" x2="-0.85" y2="0.425" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.425" x2="0.85" y2="0.425" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.425" x2="0.85" y2="-0.425" width="0.12" layer="51"/>
<smd name="1" x="-0.825" y="0" dx="0.7791" dy="0.9" layer="1" roundness="50"/>
<smd name="2" x="0.825" y="0" dx="0.7791" dy="0.9" layer="1" roundness="50"/>
<text x="0" y="1.399" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.399" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="RESC1608X50N" urn="urn:adsk.eagle:package:17469615/1" type="model" library_version="4" library_locally_modified="yes">
<description>Chip, 1.60 X 0.80 X 0.50 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC1608X50N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R_COMMON" urn="urn:adsk.eagle:symbol:17502137/2" library_version="4" library_locally_modified="yes">
<wire x1="0" y1="2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="10.16" y2="2.032" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.032" x2="10.16" y2="-2.032" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<pin name="P$1" x="0" y="0" visible="off" length="point" direction="pas"/>
<pin name="P$2" x="10.16" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="5.08" y="3.556" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="5.08" y="-3.556" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-0603" urn="urn:adsk.eagle:component:17502138/3" locally_modified="yes" prefix="R" uservalue="yes" library_version="4" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="R_COMMON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17469615/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC-ESD" urn="urn:adsk.eagle:library:17541816">
<packages>
<package name="SOT95P280X145-6N" urn="urn:adsk.eagle:footprint:17541804/1" library_version="1">
<description>6-SOT23, 0.95 mm pitch, 2.80 mm span, 2.92 X 1.63 X 1.45 mm body
&lt;p&gt;6-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.92 X 1.63 X 1.45 mm&lt;/p&gt;</description>
<circle x="-1.379" y="1.7525" radius="0.25" width="0" layer="21"/>
<wire x1="-0.875" y1="1.5625" x2="0.875" y2="1.5625" width="0.12" layer="21"/>
<wire x1="-0.875" y1="-1.5625" x2="0.875" y2="-1.5625" width="0.12" layer="21"/>
<wire x1="0.875" y1="-1.525" x2="-0.875" y2="-1.525" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-1.525" x2="-0.875" y2="1.525" width="0.12" layer="51"/>
<wire x1="-0.875" y1="1.525" x2="0.875" y2="1.525" width="0.12" layer="51"/>
<wire x1="0.875" y1="1.525" x2="0.875" y2="-1.525" width="0.12" layer="51"/>
<smd name="1" x="-1.2754" y="0.95" dx="1.1646" dy="0.5971" layer="1" roundness="100"/>
<smd name="2" x="-1.2754" y="0" dx="1.1646" dy="0.5971" layer="1" roundness="100"/>
<smd name="3" x="-1.2754" y="-0.95" dx="1.1646" dy="0.5971" layer="1" roundness="100"/>
<smd name="4" x="1.2754" y="-0.95" dx="1.1646" dy="0.5971" layer="1" roundness="100"/>
<smd name="5" x="1.2754" y="0" dx="1.1646" dy="0.5971" layer="1" roundness="100"/>
<smd name="6" x="1.2754" y="0.95" dx="1.1646" dy="0.5971" layer="1" roundness="100"/>
<text x="0" y="2.6375" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.1975" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOT95P280X145-6N" urn="urn:adsk.eagle:package:17541801/1" type="model" library_version="1">
<description>6-SOT23, 0.95 mm pitch, 2.80 mm span, 2.92 X 1.63 X 1.45 mm body
&lt;p&gt;6-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.92 X 1.63 X 1.45 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X145-6N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="USBLC6-2SC6" urn="urn:adsk.eagle:symbol:17541817/1" library_version="1">
<pin name="I/O1@1" x="0" y="0" length="middle"/>
<pin name="GND" x="0" y="-5.08" length="middle" direction="pwr"/>
<pin name="I/O2@1" x="0" y="-10.16" length="middle"/>
<pin name="I/O2@2" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="VBUS" x="30.48" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="I/O1@2" x="30.48" y="0" length="middle" rot="R180"/>
<wire x1="5.08" y1="5.08" x2="25.4" y2="5.08" width="0.254" layer="94"/>
<wire x1="25.4" y1="5.08" x2="25.4" y2="-15.24" width="0.254" layer="94"/>
<wire x1="25.4" y1="-15.24" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="-15.24" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<text x="15.24" y="5.588" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="15.24" y="-15.748" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="USBLC6-2SC6" urn="urn:adsk.eagle:component:17541818/1" prefix="IC" library_version="1">
<gates>
<gate name="G$1" symbol="USBLC6-2SC6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-6N">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="I/O1@1" pad="1"/>
<connect gate="G$1" pin="I/O1@2" pad="6"/>
<connect gate="G$1" pin="I/O2@1" pad="3"/>
<connect gate="G$1" pin="I/O2@2" pad="4"/>
<connect gate="G$1" pin="VBUS" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17541801/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="C" urn="urn:adsk.eagle:library:17502250">
<packages>
<package name="CAPC1608X90N" urn="urn:adsk.eagle:footprint:17470104/1" library_version="1" library_locally_modified="yes">
<description>Chip, 1.60 X 0.80 X 0.90 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.90 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="0.764" x2="-0.85" y2="0.764" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.764" x2="-0.85" y2="-0.764" width="0.12" layer="21"/>
<wire x1="0.85" y1="-0.425" x2="-0.85" y2="-0.425" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.425" x2="-0.85" y2="0.425" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.425" x2="0.85" y2="0.425" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.425" x2="0.85" y2="-0.425" width="0.12" layer="51"/>
<smd name="1" x="-0.825" y="0" dx="0.7791" dy="0.9" layer="1" roundness="50"/>
<smd name="2" x="0.825" y="0" dx="0.7791" dy="0.9" layer="1" roundness="50"/>
<text x="0" y="1.399" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.399" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="CAPC1608X90N" urn="urn:adsk.eagle:package:17470101/1" type="model" library_version="1" library_locally_modified="yes">
<description>Chip, 1.60 X 0.80 X 0.90 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.90 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X90N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C_COMMON" urn="urn:adsk.eagle:symbol:17502251/1" locally_modified="yes" library_version="1" library_locally_modified="yes">
<wire x1="0.762" y1="4.064" x2="0.762" y2="-4.064" width="0.254" layer="94"/>
<wire x1="1.778" y1="4.064" x2="1.778" y2="-4.064" width="0.254" layer="94"/>
<pin name="P$1" x="0.762" y="0" visible="off" length="point" direction="pas"/>
<pin name="P$2" x="1.778" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="1.27" y="5.588" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="1.27" y="-5.588" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-0603" urn="urn:adsk.eagle:component:17502252/1" locally_modified="yes" prefix="С" uservalue="yes" library_version="1" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="C_COMMON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPC1608X90N">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17470101/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_Memory" urn="urn:adsk.eagle:library:17528803">
<packages>
<package name="SOIC127P798X216-8N" urn="urn:adsk.eagle:footprint:17528811/1" library_version="3">
<description>8-SOIC, 1.27 mm pitch, 7.98 mm span, 5.24 X 5.29 X 2.16 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 7.98 mm span with body size 5.24 X 5.29 X 2.16 mm&lt;/p&gt;</description>
<circle x="-3.5928" y="2.6997" radius="0.25" width="0" layer="21"/>
<wire x1="-2.7" y1="2.4497" x2="-2.7" y2="2.675" width="0.12" layer="21"/>
<wire x1="-2.7" y1="2.675" x2="2.7" y2="2.675" width="0.12" layer="21"/>
<wire x1="2.7" y1="2.675" x2="2.7" y2="2.4497" width="0.12" layer="21"/>
<wire x1="-2.7" y1="-2.4497" x2="-2.7" y2="-2.675" width="0.12" layer="21"/>
<wire x1="-2.7" y1="-2.675" x2="2.7" y2="-2.675" width="0.12" layer="21"/>
<wire x1="2.7" y1="-2.675" x2="2.7" y2="-2.4497" width="0.12" layer="21"/>
<wire x1="2.7" y1="-2.675" x2="-2.7" y2="-2.675" width="0.12" layer="51"/>
<wire x1="-2.7" y1="-2.675" x2="-2.7" y2="2.675" width="0.12" layer="51"/>
<wire x1="-2.7" y1="2.675" x2="2.7" y2="2.675" width="0.12" layer="51"/>
<wire x1="2.7" y1="2.675" x2="2.7" y2="-2.675" width="0.12" layer="51"/>
<smd name="1" x="-3.6284" y="1.905" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<smd name="2" x="-3.6284" y="0.635" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<smd name="3" x="-3.6284" y="-0.635" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<smd name="4" x="-3.6284" y="-1.905" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<smd name="5" x="3.6284" y="-1.905" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<smd name="6" x="3.6284" y="-0.635" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<smd name="7" x="3.6284" y="0.635" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<smd name="8" x="3.6284" y="1.905" dx="1.7143" dy="0.5815" layer="1" roundness="100"/>
<text x="0" y="3.5847" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.31" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOIC127P798X216-8N" urn="urn:adsk.eagle:package:17528804/1" type="model" library_version="3">
<description>8-SOIC, 1.27 mm pitch, 7.98 mm span, 5.24 X 5.29 X 2.16 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 7.98 mm span with body size 5.24 X 5.29 X 2.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P798X216-8N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="AT45DB641E" urn="urn:adsk.eagle:symbol:17528828/1" locally_modified="yes" library_version="3" library_locally_modified="yes">
<pin name="MOSI" x="0" y="0" length="middle" direction="in"/>
<pin name="SCK" x="0" y="-5.08" length="middle" direction="in" function="clk"/>
<pin name="RESET" x="0" y="-10.16" length="middle" direction="in" function="dot"/>
<pin name="CS" x="0" y="-15.24" length="middle" direction="in" function="dot"/>
<pin name="WP" x="30.48" y="-15.24" length="middle" direction="in" function="dot" rot="R180"/>
<pin name="VCC" x="30.48" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="30.48" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="MISO" x="30.48" y="0" length="middle" direction="out" rot="R180"/>
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="2.54" x2="25.4" y2="-17.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="-17.78" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="-17.78" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="15.24" y="3.302" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="15.24" y="-18.288" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AT45DB641E" urn="urn:adsk.eagle:component:17528829/2" locally_modified="yes" prefix="IC" uservalue="yes" library_version="3" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="AT45DB641E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P798X216-8N">
<connects>
<connect gate="G$1" pin="CS" pad="4"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="MISO" pad="8"/>
<connect gate="G$1" pin="MOSI" pad="1"/>
<connect gate="G$1" pin="RESET" pad="3"/>
<connect gate="G$1" pin="SCK" pad="2"/>
<connect gate="G$1" pin="VCC" pad="6"/>
<connect gate="G$1" pin="WP" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17528804/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC-MCU" urn="urn:adsk.eagle:library:17495440">
<packages>
<package name="QFP80P900X900X160-32N" urn="urn:adsk.eagle:footprint:17469869/1" library_version="3">
<description>32-QFP, 0.80 mm pitch, 9.00 mm span, 7.00 X 7.00 X 1.60 mm body
&lt;p&gt;32-pin QFP package with 0.80 mm pitch, 9.00 mm lead span1 X 9.00 mm lead span2 with body size 7.00 X 7.00 X 1.60 mm&lt;/p&gt;</description>
<circle x="-4.2788" y="3.5775" radius="0.25" width="0" layer="21"/>
<wire x1="-3.6" y1="3.3275" x2="-3.6" y2="3.6" width="0.12" layer="21"/>
<wire x1="-3.6" y1="3.6" x2="-3.3275" y2="3.6" width="0.12" layer="21"/>
<wire x1="3.6" y1="3.3275" x2="3.6" y2="3.6" width="0.12" layer="21"/>
<wire x1="3.6" y1="3.6" x2="3.3275" y2="3.6" width="0.12" layer="21"/>
<wire x1="3.6" y1="-3.3275" x2="3.6" y2="-3.6" width="0.12" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.3275" y2="-3.6" width="0.12" layer="21"/>
<wire x1="-3.6" y1="-3.3275" x2="-3.6" y2="-3.6" width="0.12" layer="21"/>
<wire x1="-3.6" y1="-3.6" x2="-3.3275" y2="-3.6" width="0.12" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="-3.6" y2="-3.6" width="0.12" layer="51"/>
<wire x1="-3.6" y1="-3.6" x2="-3.6" y2="3.6" width="0.12" layer="51"/>
<wire x1="-3.6" y1="3.6" x2="3.6" y2="3.6" width="0.12" layer="51"/>
<wire x1="3.6" y1="3.6" x2="3.6" y2="-3.6" width="0.12" layer="51"/>
<smd name="1" x="-4.1783" y="2.8" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="2" x="-4.1783" y="2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="3" x="-4.1783" y="1.2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="4" x="-4.1783" y="0.4" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="5" x="-4.1783" y="-0.4" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="6" x="-4.1783" y="-1.2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="7" x="-4.1783" y="-2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="8" x="-4.1783" y="-2.8" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="9" x="-2.8" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="-2" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="-1.2" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="-0.4" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="0.4" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="1.2" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="2" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="2.8" y="-4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="4.1783" y="-2.8" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="18" x="4.1783" y="-2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="19" x="4.1783" y="-1.2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="20" x="4.1783" y="-0.4" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="21" x="4.1783" y="0.4" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="22" x="4.1783" y="1.2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="23" x="4.1783" y="2" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="24" x="4.1783" y="2.8" dx="1.5588" dy="0.5471" layer="1" roundness="100"/>
<smd name="25" x="2.8" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="26" x="2" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="1.2" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="28" x="0.4" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="-0.4" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="30" x="-1.2" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="-2" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<smd name="32" x="-2.8" y="4.1783" dx="1.5588" dy="0.5471" layer="1" roundness="100" rot="R90"/>
<text x="0" y="5.5927" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.5927" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="QFP80P900X900X160-32N" urn="urn:adsk.eagle:package:17469856/1" type="model" library_version="3">
<description>32-QFP, 0.80 mm pitch, 9.00 mm span, 7.00 X 7.00 X 1.60 mm body
&lt;p&gt;32-pin QFP package with 0.80 mm pitch, 9.00 mm lead span1 X 9.00 mm lead span2 with body size 7.00 X 7.00 X 1.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFP80P900X900X160-32N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="STM32L052K8T6" urn="urn:adsk.eagle:symbol:17495445/2" library_version="3">
<pin name="VDD@1" x="0" y="0" length="middle" direction="pwr"/>
<pin name="PC14" x="0" y="-5.08" length="middle"/>
<pin name="PC15" x="0" y="-10.16" length="middle"/>
<pin name="NRST" x="0" y="-15.24" length="middle"/>
<pin name="VDDA" x="0" y="-20.32" length="middle"/>
<pin name="PA0" x="0" y="-25.4" length="middle"/>
<pin name="PA1" x="0" y="-30.48" length="middle"/>
<pin name="PA2" x="0" y="-35.56" length="middle"/>
<pin name="PA3" x="20.32" y="-55.88" length="middle" rot="R90"/>
<pin name="PA4" x="25.4" y="-55.88" length="middle" rot="R90"/>
<pin name="PA5" x="30.48" y="-55.88" length="middle" rot="R90"/>
<pin name="PA6" x="35.56" y="-55.88" length="middle" rot="R90"/>
<pin name="PA7" x="40.64" y="-55.88" length="middle" rot="R90"/>
<pin name="PB0" x="45.72" y="-55.88" length="middle" rot="R90"/>
<pin name="PB1" x="50.8" y="-55.88" length="middle" rot="R90"/>
<pin name="VSS@1" x="55.88" y="-55.88" length="middle" rot="R90"/>
<pin name="VDD@2" x="76.2" y="-35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="PA8" x="76.2" y="-30.48" length="middle" rot="R180"/>
<pin name="PA9" x="76.2" y="-25.4" length="middle" rot="R180"/>
<pin name="PA10" x="76.2" y="-20.32" length="middle" rot="R180"/>
<pin name="PA11" x="76.2" y="-15.24" length="middle" rot="R180"/>
<pin name="PA12" x="76.2" y="-10.16" length="middle" rot="R180"/>
<pin name="PA13" x="76.2" y="-5.08" length="middle" rot="R180"/>
<pin name="PA14" x="76.2" y="0" length="middle" rot="R180"/>
<pin name="PA15" x="55.88" y="20.32" length="middle" rot="R270"/>
<pin name="PB3" x="50.8" y="20.32" length="middle" rot="R270"/>
<pin name="PB4" x="45.72" y="20.32" length="middle" rot="R270"/>
<pin name="PB5" x="40.64" y="20.32" length="middle" rot="R270"/>
<pin name="PB6" x="35.56" y="20.32" length="middle" rot="R270"/>
<pin name="PB7" x="30.48" y="20.32" length="middle" rot="R270"/>
<pin name="BOOT0" x="25.4" y="20.32" length="middle" rot="R270"/>
<pin name="VSS@2" x="20.32" y="20.32" length="middle" rot="R270"/>
<wire x1="5.08" y1="15.24" x2="71.12" y2="15.24" width="0.254" layer="94"/>
<wire x1="71.12" y1="15.24" x2="71.12" y2="-50.8" width="0.254" layer="94"/>
<wire x1="71.12" y1="-50.8" x2="5.08" y2="-50.8" width="0.254" layer="94"/>
<wire x1="5.08" y1="-50.8" x2="5.08" y2="15.24" width="0.254" layer="94"/>
<text x="5.08" y="15.748" size="1.778" layer="95">&gt;NAME</text>
<text x="38.1" y="-17.78" size="1.778" layer="96" align="center">STM32L052K8T6</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32L052K8T6" urn="urn:adsk.eagle:component:17495586/2" prefix="IC" library_version="3">
<gates>
<gate name="G$1" symbol="STM32L052K8T6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP80P900X900X160-32N">
<connects>
<connect gate="G$1" pin="BOOT0" pad="31"/>
<connect gate="G$1" pin="NRST" pad="4"/>
<connect gate="G$1" pin="PA0" pad="6"/>
<connect gate="G$1" pin="PA1" pad="7"/>
<connect gate="G$1" pin="PA10" pad="20"/>
<connect gate="G$1" pin="PA11" pad="21"/>
<connect gate="G$1" pin="PA12" pad="22"/>
<connect gate="G$1" pin="PA13" pad="23"/>
<connect gate="G$1" pin="PA14" pad="24"/>
<connect gate="G$1" pin="PA15" pad="25"/>
<connect gate="G$1" pin="PA2" pad="8"/>
<connect gate="G$1" pin="PA3" pad="9"/>
<connect gate="G$1" pin="PA4" pad="10"/>
<connect gate="G$1" pin="PA5" pad="11"/>
<connect gate="G$1" pin="PA6" pad="12"/>
<connect gate="G$1" pin="PA7" pad="13"/>
<connect gate="G$1" pin="PA8" pad="18"/>
<connect gate="G$1" pin="PA9" pad="19"/>
<connect gate="G$1" pin="PB0" pad="14"/>
<connect gate="G$1" pin="PB1" pad="15"/>
<connect gate="G$1" pin="PB3" pad="26"/>
<connect gate="G$1" pin="PB4" pad="27"/>
<connect gate="G$1" pin="PB5" pad="28"/>
<connect gate="G$1" pin="PB6" pad="29"/>
<connect gate="G$1" pin="PB7" pad="30"/>
<connect gate="G$1" pin="PC14" pad="2"/>
<connect gate="G$1" pin="PC15" pad="3"/>
<connect gate="G$1" pin="VDD@1" pad="1"/>
<connect gate="G$1" pin="VDD@2" pad="17"/>
<connect gate="G$1" pin="VDDA" pad="5"/>
<connect gate="G$1" pin="VSS@1" pad="16"/>
<connect gate="G$1" pin="VSS@2" pad="32"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17469856/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC-LDO" urn="urn:adsk.eagle:library:17535190">
<packages>
<package name="SOT95P280X145-5N" urn="urn:adsk.eagle:footprint:17535194/1" library_version="2">
<description>5-SOT23, 0.95 mm pitch, 2.80 mm span, 2.90 X 1.60 X 1.45 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.90 X 1.60 X 1.45 mm&lt;/p&gt;</description>
<circle x="-1.379" y="1.7486" radius="0.25" width="0" layer="21"/>
<wire x1="-0.875" y1="1.5586" x2="0.875" y2="1.5586" width="0.12" layer="21"/>
<wire x1="-0.875" y1="-1.5586" x2="0.875" y2="-1.5586" width="0.12" layer="21"/>
<wire x1="0.875" y1="-1.525" x2="-0.875" y2="-1.525" width="0.12" layer="51"/>
<wire x1="-0.875" y1="-1.525" x2="-0.875" y2="1.525" width="0.12" layer="51"/>
<wire x1="-0.875" y1="1.525" x2="0.875" y2="1.525" width="0.12" layer="51"/>
<wire x1="0.875" y1="1.525" x2="0.875" y2="-1.525" width="0.12" layer="51"/>
<smd name="1" x="-1.2533" y="0.95" dx="1.2088" dy="0.5891" layer="1" roundness="100"/>
<smd name="2" x="-1.2533" y="0" dx="1.2088" dy="0.5891" layer="1" roundness="100"/>
<smd name="3" x="-1.2533" y="-0.95" dx="1.2088" dy="0.5891" layer="1" roundness="100"/>
<smd name="4" x="1.2533" y="-0.95" dx="1.2088" dy="0.5891" layer="1" roundness="100"/>
<smd name="5" x="1.2533" y="0.95" dx="1.2088" dy="0.5891" layer="1" roundness="100"/>
<text x="0" y="2.6336" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.1936" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOT95P280X145-5N" urn="urn:adsk.eagle:package:17535193/1" type="model" library_version="2">
<description>5-SOT23, 0.95 mm pitch, 2.80 mm span, 2.90 X 1.60 X 1.45 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.80 mm span with body size 2.90 X 1.60 X 1.45 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P280X145-5N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LP5907" urn="urn:adsk.eagle:symbol:17535191/1" library_version="2">
<pin name="IN" x="0" y="0" length="middle"/>
<pin name="EN" x="0" y="-5.08" length="middle"/>
<pin name="GND" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="OUT" x="25.4" y="0" length="middle" rot="R180"/>
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="2.54" x2="20.32" y2="-7.62" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="12.7" y="3.302" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="12.7" y="-8.636" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LP5907MFX-3.3" urn="urn:adsk.eagle:component:17535196/1" prefix="IC" library_version="2">
<gates>
<gate name="G$1" symbol="LP5907" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-5N">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17535193/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="L" urn="urn:adsk.eagle:library:17570044">
<packages>
<package name="INDM2925X240N" urn="urn:adsk.eagle:footprint:17570040/1" library_version="1" library_locally_modified="yes">
<description>Molded Body, 2.90 X 2.50 X 2.40 mm body
&lt;p&gt;Molded Body package with body size 2.90 X 2.50 X 2.40 mm&lt;/p&gt;</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.12" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="1.55" y2="-1.35" width="0.12" layer="21"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.12" layer="51"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.12" layer="51"/>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.12" layer="51"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.12" layer="51"/>
<smd name="1" x="-1.2599" y="0" dx="1.5955" dy="1.1153" layer="1"/>
<smd name="2" x="1.2599" y="0" dx="1.5955" dy="1.1153" layer="1"/>
<text x="0" y="1.985" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="INDM2925X240N" urn="urn:adsk.eagle:package:17570035/1" type="model" library_version="1" library_locally_modified="yes">
<description>Molded Body, 2.90 X 2.50 X 2.40 mm body
&lt;p&gt;Molded Body package with body size 2.90 X 2.50 X 2.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="INDM2925X240N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="L_COMMON" urn="urn:adsk.eagle:symbol:17570045/1" locally_modified="yes" library_version="1" library_locally_modified="yes">
<wire x1="0" y1="0" x2="5.08" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="0" x2="10.16" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="10.16" y1="0" x2="15.24" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="15.24" y1="0" x2="20.32" y2="0" width="0.254" layer="94" curve="-180"/>
<pin name="P$1" x="0" y="0" visible="off" length="point" direction="pas"/>
<pin name="P$2" x="20.32" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="10.16" y="3.048" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="10.16" y="-0.508" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CM322522" urn="urn:adsk.eagle:component:17570046/1" prefix="L" uservalue="yes" library_version="1" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="L_COMMON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="INDM2925X240N">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17570035/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="X-PLS" urn="urn:adsk.eagle:library:17637095">
<packages>
<package name="HDRV4W64P254_1X4_1016X254X864B" urn="urn:adsk.eagle:footprint:17637083/1" library_version="1">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 6.10 mm mating length, 10.16 X 2.54 X 8.64 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 6.10 mm mating length with overall size 10.16 X 2.54 X 8.64 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="1.774" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="8.89" y2="1.27" width="0.12" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="-1.27" width="0.12" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="8.89" y2="1.27" width="0.12" layer="51"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="-1.27" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="1.7051"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="1.7051"/>
<text x="0" y="2.659" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="HDRV4W64P254_1X4_1016X254X864B" urn="urn:adsk.eagle:package:17637080/1" type="model" library_version="1">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 6.10 mm mating length, 10.16 X 2.54 X 8.64 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 6.10 mm mating length with overall size 10.16 X 2.54 X 8.64 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W64P254_1X4_1016X254X864B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="4-PIN" urn="urn:adsk.eagle:symbol:17637096/1" library_version="1">
<wire x1="7.62" y1="0" x2="5.08" y2="1.524" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="5.08" y2="-1.524" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="5.08" y2="-3.556" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="5.08" y2="-6.604" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="5.08" y2="-8.636" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="5.08" y2="-11.684" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="5.08" y2="-13.716" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="5.08" y2="-16.764" width="0.254" layer="94"/>
<wire x1="3.556" y1="0" x2="3.556" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<pin name="P$1" x="0" y="0" visible="pad"/>
<pin name="P$2" x="0" y="-5.08" visible="pad"/>
<pin name="P$3" x="0" y="-10.16" visible="pad"/>
<pin name="P$4" x="0" y="-15.24" visible="pad"/>
<text x="2.54" y="2.54" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="2.54" y="-20.32" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PLS-4" urn="urn:adsk.eagle:component:17637097/1" prefix="X" library_version="1">
<gates>
<gate name="G$1" symbol="4-PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV4W64P254_1X4_1016X254X864B">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17637080/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_NFC" urn="urn:adsk.eagle:library:17734884">
<packages>
<package name="SOP65P640X120-8N" urn="urn:adsk.eagle:footprint:17734874/1" library_version="1">
<description>8-SOP, 0.65 mm pitch, 6.40 mm span, 3.00 X 4.40 X 1.20 mm body
&lt;p&gt;8-pin SOP package with 0.65 mm pitch, 6.40 mm span with body size 3.00 X 4.40 X 1.20 mm&lt;/p&gt;</description>
<circle x="-2.9538" y="1.6824" radius="0.25" width="0" layer="21"/>
<wire x1="-2.25" y1="1.4324" x2="-2.25" y2="1.55" width="0.12" layer="21"/>
<wire x1="-2.25" y1="1.55" x2="2.25" y2="1.55" width="0.12" layer="21"/>
<wire x1="2.25" y1="1.55" x2="2.25" y2="1.4324" width="0.12" layer="21"/>
<wire x1="-2.25" y1="-1.4324" x2="-2.25" y2="-1.55" width="0.12" layer="21"/>
<wire x1="-2.25" y1="-1.55" x2="2.25" y2="-1.55" width="0.12" layer="21"/>
<wire x1="2.25" y1="-1.55" x2="2.25" y2="-1.4324" width="0.12" layer="21"/>
<wire x1="2.25" y1="-1.55" x2="-2.25" y2="-1.55" width="0.12" layer="51"/>
<wire x1="-2.25" y1="-1.55" x2="-2.25" y2="1.55" width="0.12" layer="51"/>
<wire x1="-2.25" y1="1.55" x2="2.25" y2="1.55" width="0.12" layer="51"/>
<wire x1="2.25" y1="1.55" x2="2.25" y2="-1.55" width="0.12" layer="51"/>
<smd name="1" x="-2.866" y="0.975" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<smd name="2" x="-2.866" y="0.325" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<smd name="3" x="-2.866" y="-0.325" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<smd name="4" x="-2.866" y="-0.975" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<smd name="5" x="2.866" y="-0.975" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<smd name="6" x="2.866" y="-0.325" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<smd name="7" x="2.866" y="0.325" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<smd name="8" x="2.866" y="0.975" dx="1.5834" dy="0.4068" layer="1" roundness="100"/>
<text x="0" y="2.5674" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.185" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SOP65P640X120-8N" urn="urn:adsk.eagle:package:17734868/1" type="model" library_version="1">
<description>8-SOP, 0.65 mm pitch, 6.40 mm span, 3.00 X 4.40 X 1.20 mm body
&lt;p&gt;8-pin SOP package with 0.65 mm pitch, 6.40 mm span with body size 3.00 X 4.40 X 1.20 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP65P640X120-8N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ST25DV04" urn="urn:adsk.eagle:symbol:17734885/1" library_version="1">
<pin name="V_EH" x="0" y="0" length="middle"/>
<pin name="AC0" x="0" y="-5.08" length="middle"/>
<pin name="AC1" x="0" y="-10.16" length="middle"/>
<pin name="VSS" x="0" y="-15.24" length="middle"/>
<pin name="SDA" x="30.48" y="-15.24" length="middle" rot="R180"/>
<pin name="SCL" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="GPO" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="VCC" x="30.48" y="0" length="middle" rot="R180"/>
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="2.54" x2="25.4" y2="-17.78" width="0.254" layer="94"/>
<wire x1="25.4" y1="-17.78" x2="5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="-17.78" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="15.24" y="3.81" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="15.24" y="-19.05" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ST25DV04" urn="urn:adsk.eagle:component:17734886/1" prefix="IC" library_version="1">
<gates>
<gate name="G$1" symbol="ST25DV04" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P640X120-8N">
<connects>
<connect gate="G$1" pin="AC0" pad="2"/>
<connect gate="G$1" pin="AC1" pad="3"/>
<connect gate="G$1" pin="GPO" pad="7"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="VSS" pad="4"/>
<connect gate="G$1" pin="V_EH" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17734868/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="VD-LED" urn="urn:adsk.eagle:library:17746243">
<packages>
<package name="LEDC1608X75N" urn="urn:adsk.eagle:footprint:17746019/1" library_version="1">
<description>Chip, 1.60 X 0.80 X 0.75 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.75 mm&lt;/p&gt;</description>
<wire x1="0.8" y1="0.7699" x2="-1.3099" y2="0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="0.7699" x2="-1.3099" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="-1.3099" y1="-0.7699" x2="0.8" y2="-0.7699" width="0.12" layer="21"/>
<wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.12" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.12" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.12" layer="51"/>
<smd name="1" x="-0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<smd name="2" x="0.75" y="0" dx="0.6118" dy="0.9118" layer="1"/>
<text x="0" y="1.4049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="LEDC1608X75N" urn="urn:adsk.eagle:package:17746012/2" type="model" library_version="1">
<description>Chip, 1.60 X 0.80 X 0.75 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC1608X75N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:17746244/1" library_version="1">
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="15.24" y="0" visible="pad" length="middle" rot="R180"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED-0603-RED" urn="urn:adsk.eagle:component:17746245/1" prefix="VD" library_version="1">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LEDC1608X75N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:17746012/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="X-USB" library_urn="urn:adsk.eagle:library:17541912" deviceset="USB-A-PCB" device="" package3d_urn="urn:adsk.eagle:package:17541915/2"/>
<part name="R1" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1" value="22"/>
<part name="R2" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1" value="22"/>
<part name="IC1" library="IC-ESD" library_urn="urn:adsk.eagle:library:17541816" deviceset="USBLC6-2SC6" device="" package3d_urn="urn:adsk.eagle:package:17541801/1"/>
<part name="С1" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="100nF"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="IC2" library="IC_Memory" library_urn="urn:adsk.eagle:library:17528803" deviceset="AT45DB641E" device="" package3d_urn="urn:adsk.eagle:package:17528804/1"/>
<part name="IC3" library="IC-MCU" library_urn="urn:adsk.eagle:library:17495440" deviceset="STM32L052K8T6" device="" package3d_urn="urn:adsk.eagle:package:17469856/1"/>
<part name="IC4" library="IC-LDO" library_urn="urn:adsk.eagle:library:17535190" deviceset="LP5907MFX-3.3" device="" package3d_urn="urn:adsk.eagle:package:17535193/1"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="С2" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="1uF"/>
<part name="С3" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="1uF"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="С4" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="10uF"/>
<part name="С5" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="100nF"/>
<part name="С6" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="100nF"/>
<part name="С7" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="1uF"/>
<part name="С8" library="C" library_urn="urn:adsk.eagle:library:17502250" deviceset="C-0603" device="" package3d_urn="urn:adsk.eagle:package:17470101/1" value="100nF"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="L1" library="L" library_urn="urn:adsk.eagle:library:17570044" deviceset="CM322522" device="" package3d_urn="urn:adsk.eagle:package:17570035/1" value="100uH"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X2" library="X-PLS" library_urn="urn:adsk.eagle:library:17637095" deviceset="PLS-4" device="" package3d_urn="urn:adsk.eagle:package:17637080/1"/>
<part name="+3V6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC5" library="IC_NFC" library_urn="urn:adsk.eagle:library:17734884" deviceset="ST25DV04" device="" package3d_urn="urn:adsk.eagle:package:17734868/1"/>
<part name="+3V7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R3" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1"/>
<part name="R4" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1"/>
<part name="R5" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1"/>
<part name="R6" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1"/>
<part name="R7" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1"/>
<part name="R8" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1"/>
<part name="VD1" library="VD-LED" library_urn="urn:adsk.eagle:library:17746243" deviceset="LED-0603-RED" device="" package3d_urn="urn:adsk.eagle:package:17746012/2"/>
<part name="VD2" library="VD-LED" library_urn="urn:adsk.eagle:library:17746243" deviceset="LED-0603-RED" device="" package3d_urn="urn:adsk.eagle:package:17746012/2"/>
<part name="VD3" library="VD-LED" library_urn="urn:adsk.eagle:library:17746243" deviceset="LED-0603-RED" device="" package3d_urn="urn:adsk.eagle:package:17746012/2"/>
<part name="VD4" library="VD-LED" library_urn="urn:adsk.eagle:library:17746243" deviceset="LED-0603-RED" device="" package3d_urn="urn:adsk.eagle:package:17746012/2"/>
<part name="VD5" library="VD-LED" library_urn="urn:adsk.eagle:library:17746243" deviceset="LED-0603-RED" device="" package3d_urn="urn:adsk.eagle:package:17746012/2"/>
<part name="VD6" library="VD-LED" library_urn="urn:adsk.eagle:library:17746243" deviceset="LED-0603-RED" device="" package3d_urn="urn:adsk.eagle:package:17746012/2"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="VD7" library="VD-LED" library_urn="urn:adsk.eagle:library:17746243" deviceset="LED-0603-RED" device="" package3d_urn="urn:adsk.eagle:package:17746012/2"/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R9" library="R" library_urn="urn:adsk.eagle:library:17502134" deviceset="R-0603" device="" package3d_urn="urn:adsk.eagle:package:17469615/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="X1" gate="G$1" x="322.58" y="114.3" smashed="yes"/>
<instance part="R1" gate="G$1" x="294.64" y="111.76" smashed="yes">
<attribute name="NAME" x="299.72" y="115.316" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="299.72" y="108.204" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R2" gate="G$1" x="294.64" y="101.6" smashed="yes">
<attribute name="NAME" x="299.72" y="105.156" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="299.72" y="98.044" size="1.778" layer="96" align="center"/>
</instance>
<instance part="IC1" gate="G$1" x="287.02" y="111.76" smashed="yes" rot="MR0">
<attribute name="NAME" x="271.78" y="94.488" size="1.778" layer="95" rot="MR0" align="bottom-center"/>
<attribute name="VALUE" x="271.78" y="118.872" size="1.778" layer="96" rot="MR0" align="top-center"/>
</instance>
<instance part="С1" gate="G$1" x="269.24" y="83.82" smashed="yes">
<attribute name="NAME" x="270.51" y="89.408" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="270.51" y="78.232" size="1.778" layer="96" align="center"/>
</instance>
<instance part="P+1" gate="1" x="317.5" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="314.96" y="114.3" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND1" gate="1" x="317.5" y="99.06" smashed="yes" rot="R270">
<attribute name="VALUE" x="314.96" y="101.6" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+2" gate="1" x="248.92" y="106.68" smashed="yes" rot="R90">
<attribute name="VALUE" x="246.888" y="106.68" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND2" gate="1" x="289.56" y="106.68" smashed="yes" rot="R90">
<attribute name="VALUE" x="292.1" y="104.14" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="281.94" y="83.82" smashed="yes" rot="R90">
<attribute name="VALUE" x="284.48" y="81.28" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="1" x="259.08" y="83.82" smashed="yes" rot="R90">
<attribute name="VALUE" x="256.54" y="83.82" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="IC2" gate="G$1" x="53.34" y="124.46" smashed="yes">
<attribute name="NAME" x="68.58" y="127.762" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="68.58" y="113.792" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="IC3" gate="G$1" x="124.46" y="121.92" smashed="yes">
<attribute name="NAME" x="129.54" y="137.668" size="1.778" layer="95"/>
</instance>
<instance part="IC4" gate="G$1" x="86.36" y="33.02" smashed="yes">
<attribute name="NAME" x="99.06" y="36.322" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="99.06" y="24.384" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="P+4" gate="1" x="66.04" y="33.02" smashed="yes" rot="R90">
<attribute name="VALUE" x="64.008" y="33.02" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="+3V1" gate="G$1" x="127" y="33.02" smashed="yes" rot="R270">
<attribute name="VALUE" x="129.54" y="33.02" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="С2" gate="G$1" x="116.84" y="29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="123.19" y="30.48" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="123.19" y="27.94" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="С3" gate="G$1" x="74.93" y="29.21" smashed="yes" rot="R90">
<attribute name="NAME" x="81.28" y="30.48" size="1.778" layer="95" rot="R180" align="center"/>
<attribute name="VALUE" x="81.28" y="27.94" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="GND4" gate="1" x="116.84" y="22.86" smashed="yes">
<attribute name="VALUE" x="114.3" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="74.93" y="22.86" smashed="yes">
<attribute name="VALUE" x="72.39" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="+3V2" gate="G$1" x="91.44" y="114.3" smashed="yes" rot="R270">
<attribute name="VALUE" x="93.98" y="114.3" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND6" gate="1" x="91.44" y="119.38" smashed="yes" rot="R90">
<attribute name="VALUE" x="93.98" y="116.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V3" gate="G$1" x="119.38" y="121.92" smashed="yes" rot="MR270">
<attribute name="VALUE" x="116.84" y="121.92" size="1.778" layer="96" rot="MR0" align="center-left"/>
</instance>
<instance part="+3V4" gate="G$1" x="205.74" y="86.36" smashed="yes" rot="R270">
<attribute name="VALUE" x="208.28" y="86.36" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND7" gate="1" x="180.34" y="60.96" smashed="yes">
<attribute name="VALUE" x="177.8" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="144.78" y="147.32" smashed="yes" rot="R180">
<attribute name="VALUE" x="144.78" y="149.86" size="1.778" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="С4" gate="G$1" x="226.06" y="50.8" smashed="yes">
<attribute name="NAME" x="227.33" y="56.388" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="227.33" y="45.212" size="1.778" layer="96" align="center"/>
</instance>
<instance part="С5" gate="G$1" x="226.06" y="35.56" smashed="yes">
<attribute name="NAME" x="227.33" y="41.148" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="227.33" y="29.972" size="1.778" layer="96" align="center"/>
</instance>
<instance part="С6" gate="G$1" x="226.06" y="20.32" smashed="yes">
<attribute name="NAME" x="227.33" y="25.908" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="227.33" y="14.732" size="1.778" layer="96" align="center"/>
</instance>
<instance part="С7" gate="G$1" x="269.24" y="35.56" smashed="yes">
<attribute name="NAME" x="270.51" y="41.148" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="270.51" y="29.972" size="1.778" layer="96" align="center"/>
</instance>
<instance part="С8" gate="G$1" x="269.24" y="20.32" smashed="yes">
<attribute name="NAME" x="270.51" y="25.908" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="270.51" y="14.732" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND10" gate="1" x="276.86" y="12.7" smashed="yes">
<attribute name="VALUE" x="274.32" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="220.98" y="12.7" smashed="yes">
<attribute name="VALUE" x="218.44" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="233.68" y="58.42" smashed="yes">
<attribute name="VALUE" x="233.68" y="59.69" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="L1" gate="G$1" x="238.76" y="35.56" smashed="yes">
<attribute name="NAME" x="248.92" y="38.608" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="248.92" y="35.052" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="P+5" gate="VCC" x="264.16" y="58.42" smashed="yes">
<attribute name="VALUE" x="261.62" y="55.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+6" gate="VCC" x="119.38" y="101.6" smashed="yes" rot="R90">
<attribute name="VALUE" x="116.84" y="101.6" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="GND11" gate="1" x="149.86" y="147.32" smashed="yes" rot="R180">
<attribute name="VALUE" x="149.86" y="149.86" size="1.778" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="X2" gate="G$1" x="322.58" y="142.24" smashed="yes">
<attribute name="NAME" x="325.12" y="144.78" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="325.12" y="121.92" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="+3V6" gate="G$1" x="317.5" y="142.24" smashed="yes" rot="MR270">
<attribute name="VALUE" x="314.96" y="142.24" size="1.778" layer="96" rot="MR0" align="center-left"/>
</instance>
<instance part="GND12" gate="1" x="317.5" y="137.16" smashed="yes" rot="R270">
<attribute name="VALUE" x="314.96" y="139.7" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="IC5" gate="G$1" x="83.82" y="5.08" smashed="yes">
<attribute name="NAME" x="99.06" y="8.89" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="99.06" y="-13.97" size="1.778" layer="96" align="center"/>
</instance>
<instance part="+3V7" gate="G$1" x="119.38" y="5.08" smashed="yes" rot="R270">
<attribute name="VALUE" x="121.92" y="5.08" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND13" gate="1" x="78.74" y="-10.16" smashed="yes" rot="R270">
<attribute name="VALUE" x="76.2" y="-7.62" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R3" gate="G$1" x="93.98" y="96.52" smashed="yes">
<attribute name="NAME" x="99.06" y="100.076" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="99.06" y="92.964" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R4" gate="G$1" x="81.28" y="91.44" smashed="yes">
<attribute name="NAME" x="86.36" y="94.996" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="86.36" y="87.884" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R5" gate="G$1" x="68.58" y="86.36" smashed="yes">
<attribute name="NAME" x="73.66" y="89.916" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="73.66" y="82.804" size="1.778" layer="96" align="center"/>
</instance>
<instance part="R6" gate="G$1" x="144.78" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="141.224" y="40.64" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="148.336" y="40.64" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R7" gate="G$1" x="165.1" y="160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="161.544" y="165.1" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="168.656" y="165.1" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="R8" gate="G$1" x="170.18" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="166.624" y="177.8" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="173.736" y="177.8" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
<instance part="VD1" gate="G$1" x="45.72" y="86.36" smashed="yes"/>
<instance part="VD2" gate="G$1" x="55.88" y="91.44" smashed="yes"/>
<instance part="VD3" gate="G$1" x="68.58" y="96.52" smashed="yes"/>
<instance part="VD4" gate="G$1" x="144.78" y="15.24" smashed="yes" rot="R90"/>
<instance part="VD5" gate="G$1" x="170.18" y="203.2" smashed="yes" rot="R270"/>
<instance part="VD6" gate="G$1" x="165.1" y="195.58" smashed="yes" rot="R270"/>
<instance part="GND14" gate="1" x="144.78" y="7.62" smashed="yes">
<attribute name="VALUE" x="142.24" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="40.64" y="86.36" smashed="yes" rot="R270">
<attribute name="VALUE" x="38.1" y="88.9" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND16" gate="1" x="45.72" y="91.44" smashed="yes" rot="R270">
<attribute name="VALUE" x="43.18" y="93.98" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND17" gate="1" x="53.34" y="96.52" smashed="yes" rot="R270">
<attribute name="VALUE" x="50.8" y="99.06" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND18" gate="1" x="170.18" y="208.28" smashed="yes" rot="R180">
<attribute name="VALUE" x="172.72" y="210.82" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND19" gate="1" x="165.1" y="200.66" smashed="yes" rot="R180">
<attribute name="VALUE" x="167.64" y="203.2" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="VD7" gate="G$1" x="180.34" y="203.2" smashed="yes" rot="R270"/>
<instance part="GND20" gate="1" x="180.34" y="208.28" smashed="yes" rot="R180">
<attribute name="VALUE" x="182.88" y="210.82" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R9" gate="G$1" x="180.34" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="176.784" y="177.8" size="1.778" layer="95" rot="R90" align="center"/>
<attribute name="VALUE" x="183.896" y="177.8" size="1.778" layer="96" rot="R90" align="center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$3" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$2"/>
<wire x1="304.8" y1="111.76" x2="312.42" y2="111.76" width="0.1524" layer="91"/>
<wire x1="312.42" y1="111.76" x2="312.42" y2="109.22" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="312.42" y1="109.22" x2="322.58" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="P$2"/>
<wire x1="304.8" y1="101.6" x2="312.42" y2="101.6" width="0.1524" layer="91"/>
<wire x1="312.42" y1="101.6" x2="312.42" y2="104.14" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="312.42" y1="104.14" x2="322.58" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<wire x1="320.04" y1="114.3" x2="322.58" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VBUS"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="251.46" y1="106.68" x2="256.54" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="С1" gate="G$1" pin="P$1"/>
<wire x1="261.62" y1="83.82" x2="270.002" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="68.58" y1="33.02" x2="74.93" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="IN"/>
<pinref part="IC4" gate="G$1" pin="EN"/>
<wire x1="74.93" y1="33.02" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
<wire x1="83.82" y1="33.02" x2="86.36" y2="33.02" width="0.1524" layer="91"/>
<wire x1="86.36" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
<wire x1="83.82" y1="27.94" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
<junction x="83.82" y="33.02"/>
<pinref part="С3" gate="G$1" pin="P$2"/>
<wire x1="74.93" y1="30.988" x2="74.93" y2="33.02" width="0.1524" layer="91"/>
<junction x="74.93" y="33.02"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="320.04" y1="99.06" x2="322.58" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="С1" gate="G$1" pin="P$2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="271.018" y1="83.82" x2="279.4" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="С3" gate="G$1" pin="P$1"/>
<wire x1="74.93" y1="25.4" x2="74.93" y2="29.972" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="116.84" y1="25.4" x2="116.84" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="111.76" y1="27.94" x2="116.84" y2="27.94" width="0.1524" layer="91"/>
<wire x1="116.84" y1="27.94" x2="116.84" y2="29.972" width="0.1524" layer="91"/>
<junction x="116.84" y="27.94"/>
<pinref part="С2" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="83.82" y1="119.38" x2="88.9" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="IC3" gate="G$1" pin="VSS@1"/>
<wire x1="180.34" y1="63.5" x2="180.34" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="VSS@2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="144.78" y1="144.78" x2="144.78" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="С6" gate="G$1" pin="P$1"/>
<wire x1="226.822" y1="20.32" x2="220.98" y2="20.32" width="0.1524" layer="91"/>
<wire x1="220.98" y1="20.32" x2="220.98" y2="35.56" width="0.1524" layer="91"/>
<pinref part="С4" gate="G$1" pin="P$1"/>
<wire x1="220.98" y1="35.56" x2="220.98" y2="50.8" width="0.1524" layer="91"/>
<wire x1="220.98" y1="50.8" x2="226.822" y2="50.8" width="0.1524" layer="91"/>
<pinref part="С5" gate="G$1" pin="P$1"/>
<wire x1="226.822" y1="35.56" x2="220.98" y2="35.56" width="0.1524" layer="91"/>
<junction x="220.98" y="35.56"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="220.98" y1="20.32" x2="220.98" y2="15.24" width="0.1524" layer="91"/>
<junction x="220.98" y="20.32"/>
</segment>
<segment>
<pinref part="С8" gate="G$1" pin="P$2"/>
<wire x1="271.018" y1="20.32" x2="276.86" y2="20.32" width="0.1524" layer="91"/>
<wire x1="276.86" y1="20.32" x2="276.86" y2="35.56" width="0.1524" layer="91"/>
<pinref part="С7" gate="G$1" pin="P$2"/>
<wire x1="276.86" y1="35.56" x2="271.018" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="276.86" y1="15.24" x2="276.86" y2="20.32" width="0.1524" layer="91"/>
<junction x="276.86" y="20.32"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="IC3" gate="G$1" pin="BOOT0"/>
<wire x1="149.86" y1="144.78" x2="149.86" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="P$2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="320.04" y1="137.16" x2="322.58" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="VSS"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="81.28" y1="-10.16" x2="83.82" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VD4" gate="G$1" pin="1"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="144.78" y1="10.16" x2="144.78" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VD1" gate="G$1" pin="1"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="43.18" y1="86.36" x2="45.72" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="VD2" gate="G$1" pin="1"/>
<wire x1="48.26" y1="91.44" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="VD3" gate="G$1" pin="1"/>
<wire x1="55.88" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="VD5" gate="G$1" pin="1"/>
<wire x1="170.18" y1="205.74" x2="170.18" y2="203.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="VD6" gate="G$1" pin="1"/>
<wire x1="165.1" y1="198.12" x2="165.1" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="VD7" gate="G$1" pin="1"/>
<wire x1="180.34" y1="205.74" x2="180.34" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="OUT"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="33.02" x2="116.84" y2="33.02" width="0.1524" layer="91"/>
<pinref part="С2" gate="G$1" pin="P$2"/>
<wire x1="116.84" y1="33.02" x2="124.46" y2="33.02" width="0.1524" layer="91"/>
<wire x1="116.84" y1="30.988" x2="116.84" y2="33.02" width="0.1524" layer="91"/>
<junction x="116.84" y="33.02"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<wire x1="83.82" y1="114.3" x2="88.9" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="VDD@1"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<wire x1="121.92" y1="121.92" x2="124.46" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<pinref part="IC3" gate="G$1" pin="VDD@2"/>
<wire x1="203.2" y1="86.36" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="С6" gate="G$1" pin="P$2"/>
<wire x1="227.838" y1="20.32" x2="233.68" y2="20.32" width="0.1524" layer="91"/>
<pinref part="С4" gate="G$1" pin="P$2"/>
<wire x1="233.68" y1="20.32" x2="233.68" y2="35.56" width="0.1524" layer="91"/>
<wire x1="233.68" y1="35.56" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
<wire x1="233.68" y1="50.8" x2="227.838" y2="50.8" width="0.1524" layer="91"/>
<pinref part="С5" gate="G$1" pin="P$2"/>
<wire x1="227.838" y1="35.56" x2="233.68" y2="35.56" width="0.1524" layer="91"/>
<junction x="233.68" y="35.56"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="233.68" y1="55.88" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
<junction x="233.68" y="50.8"/>
<pinref part="L1" gate="G$1" pin="P$1"/>
<wire x1="238.76" y1="35.56" x2="233.68" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="P$1"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="320.04" y1="142.24" x2="322.58" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="VCC"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="116.84" y1="5.08" x2="114.3" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_DM" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA11"/>
<wire x1="200.66" y1="106.68" x2="213.36" y2="106.68" width="0.1524" layer="91"/>
<label x="213.36" y="107.95" size="1.778" layer="95" rot="MR0" align="center-left"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="I/O1@2"/>
<wire x1="256.54" y1="111.76" x2="241.3" y2="111.76" width="0.1524" layer="91"/>
<label x="241.3" y="113.03" size="1.778" layer="95" align="center-left"/>
</segment>
</net>
<net name="USB_DP" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA12"/>
<wire x1="200.66" y1="111.76" x2="213.36" y2="111.76" width="0.1524" layer="91"/>
<label x="213.36" y="113.03" size="1.778" layer="95" rot="MR0" align="center-left"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="I/O2@2"/>
<wire x1="256.54" y1="101.6" x2="241.3" y2="101.6" width="0.1524" layer="91"/>
<label x="241.3" y="102.87" size="1.778" layer="95" align="center-left"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="С7" gate="G$1" pin="P$1"/>
<wire x1="270.002" y1="35.56" x2="264.16" y2="35.56" width="0.1524" layer="91"/>
<wire x1="264.16" y1="35.56" x2="264.16" y2="20.32" width="0.1524" layer="91"/>
<pinref part="С8" gate="G$1" pin="P$1"/>
<wire x1="264.16" y1="20.32" x2="270.002" y2="20.32" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="P$2"/>
<wire x1="259.08" y1="35.56" x2="264.16" y2="35.56" width="0.1524" layer="91"/>
<junction x="264.16" y="35.56"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="264.16" y1="55.88" x2="264.16" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<pinref part="IC3" gate="G$1" pin="VDDA"/>
<wire x1="121.92" y1="101.6" x2="124.46" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FLASH_MOSI" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="MOSI"/>
<wire x1="53.34" y1="124.46" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<label x="35.56" y="125.73" size="1.778" layer="95" align="center-left"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PA7"/>
<wire x1="165.1" y1="66.04" x2="165.1" y2="48.26" width="0.1524" layer="91"/>
<label x="163.83" y="48.26" size="1.778" layer="95" rot="R270" align="center-right"/>
</segment>
</net>
<net name="FLASH_SCK" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="SCK"/>
<wire x1="53.34" y1="119.38" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<label x="35.56" y="120.65" size="1.778" layer="95" align="center-left"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PA5"/>
<wire x1="154.94" y1="66.04" x2="154.94" y2="48.26" width="0.1524" layer="91"/>
<label x="153.67" y="48.26" size="1.778" layer="95" rot="R270" align="center-right"/>
</segment>
</net>
<net name="FLASH_RESET" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="RESET"/>
<wire x1="53.34" y1="114.3" x2="35.56" y2="114.3" width="0.1524" layer="91"/>
<label x="35.56" y="115.57" size="1.778" layer="95" align="center-left"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PA4"/>
<wire x1="149.86" y1="66.04" x2="149.86" y2="48.26" width="0.1524" layer="91"/>
<label x="148.59" y="48.26" size="1.778" layer="95" rot="R270" align="center-right"/>
</segment>
</net>
<net name="FLASH_CS" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CS"/>
<wire x1="53.34" y1="109.22" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<label x="35.56" y="110.49" size="1.778" layer="95" align="center-left"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PB0"/>
<wire x1="170.18" y1="66.04" x2="170.18" y2="48.26" width="0.1524" layer="91"/>
<label x="168.91" y="48.26" size="1.778" layer="95" rot="R90" align="center-left"/>
</segment>
</net>
<net name="FLASH_MISO" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="MISO"/>
<wire x1="83.82" y1="124.46" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<label x="101.6" y="125.73" size="1.778" layer="95" rot="MR0" align="center-left"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PA6"/>
<wire x1="160.02" y1="66.04" x2="160.02" y2="48.26" width="0.1524" layer="91"/>
<label x="158.75" y="48.26" size="1.778" layer="95" rot="R270" align="center-right"/>
</segment>
</net>
<net name="FLASH_WP" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="WP"/>
<wire x1="83.82" y1="109.22" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<label x="101.6" y="110.49" size="1.778" layer="95" rot="MR0" align="center-left"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="PB1"/>
<wire x1="175.26" y1="66.04" x2="175.26" y2="48.26" width="0.1524" layer="91"/>
<label x="173.99" y="48.26" size="1.778" layer="95" rot="R90" align="center-left"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="I/O1@1"/>
<pinref part="R1" gate="G$1" pin="P$1"/>
<wire x1="287.02" y1="111.76" x2="294.64" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="P$1"/>
<pinref part="IC1" gate="G$1" pin="I/O2@1"/>
<wire x1="294.64" y1="101.6" x2="287.02" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SYS_SWDIO" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA13"/>
<wire x1="200.66" y1="116.84" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<label x="215.9" y="118.11" size="1.778" layer="95" align="center-right"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="P$3"/>
<wire x1="322.58" y1="132.08" x2="307.34" y2="132.08" width="0.1524" layer="91"/>
<label x="307.34" y="133.35" size="1.778" layer="95" align="center-left"/>
</segment>
</net>
<net name="SYS_SWCLK" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA14"/>
<wire x1="200.66" y1="121.92" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<label x="215.9" y="123.19" size="1.778" layer="95" align="center-right"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="P$4"/>
<wire x1="322.58" y1="127" x2="307.34" y2="127" width="0.1524" layer="91"/>
<label x="307.34" y="128.27" size="1.778" layer="95" align="center-left"/>
</segment>
</net>
<net name="NFC_SDA" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PB7"/>
<wire x1="154.94" y1="142.24" x2="154.94" y2="160.02" width="0.1524" layer="91"/>
<label x="153.67" y="160.02" size="1.778" layer="95" rot="R270" align="center-left"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="SDA"/>
<wire x1="114.3" y1="-10.16" x2="124.46" y2="-10.16" width="0.1524" layer="91"/>
<label x="124.46" y="-8.89" size="1.778" layer="95" align="center-right"/>
</segment>
</net>
<net name="NFC_SCL" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PB6"/>
<wire x1="160.02" y1="142.24" x2="160.02" y2="160.02" width="0.1524" layer="91"/>
<label x="158.75" y="160.02" size="1.778" layer="95" rot="R270" align="center-left"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="SCL"/>
<wire x1="114.3" y1="-5.08" x2="124.46" y2="-5.08" width="0.1524" layer="91"/>
<label x="124.46" y="-3.81" size="1.778" layer="95" align="center-right"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="AC0"/>
<wire x1="83.82" y1="0" x2="71.12" y2="0" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="AC1"/>
<wire x1="83.82" y1="-5.08" x2="71.12" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="0" x2="71.12" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA0"/>
<wire x1="124.46" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA1"/>
<wire x1="124.46" y1="91.44" x2="91.44" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA2"/>
<pinref part="R5" gate="G$1" pin="P$2"/>
<wire x1="78.74" y1="86.36" x2="124.46" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PA3"/>
<wire x1="144.78" y1="66.04" x2="144.78" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PB5"/>
<wire x1="165.1" y1="142.24" x2="165.1" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="PB4"/>
<wire x1="170.18" y1="142.24" x2="170.18" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="P$1"/>
<pinref part="R9" gate="G$1" pin="P$1"/>
<wire x1="170.18" y1="165.1" x2="170.18" y2="172.72" width="0.1524" layer="91"/>
<wire x1="170.18" y1="165.1" x2="180.34" y2="165.1" width="0.1524" layer="91"/>
<wire x1="180.34" y1="165.1" x2="180.34" y2="172.72" width="0.1524" layer="91"/>
<junction x="170.18" y="165.1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="VD1" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="P$1"/>
<wire x1="60.96" y1="86.36" x2="68.58" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="VD2" gate="G$1" pin="2"/>
<pinref part="R4" gate="G$1" pin="P$1"/>
<wire x1="71.12" y1="91.44" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="VD3" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="P$1"/>
<wire x1="83.82" y1="96.52" x2="93.98" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="P$1"/>
<pinref part="VD4" gate="G$1" pin="2"/>
<wire x1="144.78" y1="30.48" x2="144.78" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="P$2"/>
<pinref part="VD5" gate="G$1" pin="2"/>
<wire x1="170.18" y1="187.96" x2="170.18" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="P$2"/>
<pinref part="VD6" gate="G$1" pin="2"/>
<wire x1="165.1" y1="180.34" x2="165.1" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="VD7" gate="G$1" pin="2"/>
<wire x1="180.34" y1="187.96" x2="180.34" y2="182.88" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="P$2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="9.0" severity="warning">
Since Version 9.0, EAGLE supports the align property for labels. 
Labels in schematic will not be understood with this version. Update EAGLE to the latest version 
for full support of labels. 
</note>
</compatibility>
</eagle>
